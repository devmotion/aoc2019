using DelimitedFiles

function fuel(mass)
    f = (mass ÷ 3) - 2

    f ≤ 0 && return 0

    return f + fuel(f)
end

modules = readdlm("input")

println("Total required fuel is ", sum(fuel, modules))