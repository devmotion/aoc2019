using DelimitedFiles

fuel(mass) = (mass ÷ 3) - 2

modules = readdlm("input")

println("Fuel requirement is ", sum(fuel, modules))